﻿using GoogleMobileAds.Api;
using System;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {

    public GameObject active;
    public GameObject menu;
    public GameObject over;
    public GameObject question;
    public GameObject staged;
    public GameObject a2;
    public GameObject a4;
    public GameObject a6;
    public GameObject a9;

    public AudioSource idle;
    public AudioSource playing;
    public AudioSource right;
    public AudioSource fail;
    public AudioSource win;

    private readonly System.Random r = new System.Random();
    public int upperbound = 100;
    public int stage = 0;
    private int time = -1;
    private int wait = -1;
    private int state = 0; // 0 = wrong, 1 = right, 2 = win
    private bool animating = false;
    private int answer = -1;
    private int[] stagesetting = new int[] { 2, 2, 4, 4, 6, 6, 9, 9 };

    private BannerView bannerView;

    // Use this for initialization
    void Start()
    {
#if UNITY_ANDROID
        string appId = "ca-app-pub-7354315263788743~5041757433";
#elif UNITY_IPHONE
        string appId = "ca-app-pub-3940256099942544~1458002511";
#else
        string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
        active = menu;
        idle.Play();
        RequestBanner();
    }

    private void RequestBanner()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-7354315263788743/4993655973";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
#else
            string adUnitId = "unexpected_platform";
#endif

        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();

        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        bannerView.LoadAd(request);
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        //menu.GetComponentsInChildren<Text>()[1].text = "Banner loaded ";
        // Handle the ad failed to load event.
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        //menu.GetComponentsInChildren<Text>()[1].text = "Banner failed to load: " + args.Message;
        // Handle the ad failed to load event.
    }

    // Update is called once per frame
    void Update () {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) animating = false;
        if (stage == 0) //main menu
        {
            if (!animating && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                var touchDelta = Input.GetTouch(0).deltaPosition;
                if (touchDelta.y > 5)
                { //flick up
                    animating = true;
                    stage = 10; // 1-1
                    PrepStage();
                    return;
                }
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            animating = true;
            active.GetComponent<Animation>().Play("SlideUp");
            menu.GetComponent<Animation>().Play("SlideIn");
            active = menu;
            stage = 0;
            time = 0;
            idle.Play();
            playing.Stop();
        }

        if (wait > 0)
        {
            wait--;
            if (wait == 0)
            {
                if (state == 0)
                {
                    GameOver("Wrong answer on stage " + (stage / 10) + "-" + ((stage % 10) + 1));
                    return;
                }
                if (state == 1)
                {
                    stage++;
                    if (stage % 10 == 0)
                    {
                        PrepStage();
                        return;
                    }
                    time = 180 + 120 * ((stage / 10) % 2) + 60;

                    GenerateQuestion();
                    question.GetComponentsInChildren<Text>()[0].text = (stage / 10) + "-" + ((stage % 10) + 1);
                    question.GetComponentsInChildren<Text>()[1].text = "5";

                    active.GetComponent<Animation>().Play("SlideUp");
                    question.GetComponent<Animation>().Play("SlideIn");
                    active = question;
                    return;
                }
                if (state == 2)
                {
                    GameOver("You Won!");
                    return;
                }
                GameOver("Error!");
            }
            return;
        }
        if (active == staged)
        {
            if (!animating && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                var touchDelta = Input.GetTouch(0).deltaPosition;
                if (touchDelta.y > 5)
                { //flick up
                    animating = true;
                    StartStage();
                    return;
                }
            }
            return;
        }

        if (stage > 0)
        {
            time--;
            if (active == question)
            {
                if (time == 0)
                {
                    GenerateAnswer();
                    return;
                }
                if (!animating && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    var touchDelta = Input.GetTouch(0).deltaPosition;
                    if (touchDelta.y > 5)
                    { //flick up
                        animating = true;
                        GenerateAnswer();
                        return;
                    }
                }
            }
            else
            {
                if (time == 0)
                {
                    playing.Stop();
                    fail.Play();
                    GameOver("Timeout on stage " + (stage / 10) + "-" + ((stage % 10) + 1));
                    return;
                }
            }
            active.GetComponentsInChildren<Text>()[1].text = (time / 60).ToString();
            return;
        }

        if (stage < 0)
        {
            if (!animating && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                var touchDelta = Input.GetTouch(0).deltaPosition;
                if (touchDelta.y > 5)
                { //flick up
                    animating = true;
                    over.GetComponent<Animation>().Play("SlideUp");
                    menu.GetComponent<Animation>().Play("SlideIn");
                    active = menu;
                    stage = 0;
                    idle.Play();
                    fail.Stop();
                    win.Stop();
                    return;
                }
            }
        }
	}

    private void GameOver(string v)
    {
        stage = -1;

        over.GetComponentsInChildren<Text>()[0].text = v;

        animating = true;
        active.GetComponent<Animation>().Play("SlideUp");
        over.GetComponent<Animation>().Play("SlideIn");
        active = over;
    }

    public void CheckAnswer(int answer)
    {
        if (wait > 0)
        {
            return;
        }
        var buttons = active.GetComponentsInChildren<Button>();
        for (int i = 0; i < buttons.Length; i++)
        {
            if (int.Parse(buttons[i].GetComponentsInChildren<Text>()[0].text) == this.answer)
            {
                var colors = buttons[i].GetComponentsInChildren<Image>()[0];
                colors.color = Color.green;
            } else
            {
                var colors = buttons[i].GetComponentsInChildren<Image>()[0];
                colors.color = Color.red;
            }
        }
        if (answer != this.answer)
        {
            playing.Stop();
            fail.Play();
            wait = 20;
            state = 0;
            return;
        }
        wait = 20;
        state = 1;
        if (stage + 1 == (stagesetting.Length + 1) * 10)
        {
            playing.Stop();
            win.Play();
            state = 2;
        }
        else if ((stage + 1) % 10 == 0)
        {
            win.Play();
        }
        else
        {
            right.Play();
        }
    }

    private void PrepStage()
    {
        if (stage == 10)
        {
            idle.Stop();
            playing.Play();

            staged.GetComponentsInChildren<Text>()[0].text = "Stage " + stage / 10;
        } else
        {
            staged.GetComponentsInChildren<Text>()[0].text = "Congratulation! You beat Stage " + ((stage / 10) - 1)+ "\r\nGet ready for Stage " + stage / 10;
            playing.Stop();
            win.Play();
        }

        animating = true;

        active.GetComponent<Animation>().Play("SlideUp");
        staged.GetComponent<Animation>().Play("SlideIn");
        active = staged;
    }

    private void StartStage()
    {
        if (stage != 10)
        {
            playing.Play();
        }
        animating = true;
        time = 180 + 120 * ((stage / 10) % 2) + 60;

        GenerateQuestion();
        question.GetComponentsInChildren<Text>()[0].text = (stage / 10) + "-" + ((stage % 10) + 1);
        question.GetComponentsInChildren<Text>()[1].text = "5";

        active.GetComponent<Animation>().Play("SlideUp");
        question.GetComponent<Animation>().Play("SlideIn");
        active = question;
    }

    private void GenerateQuestion()
    {
        answer = r.Next(2, upperbound);
        var one = r.Next(1, answer);
        var two = answer - one;
        question.GetComponentsInChildren<Text>()[2].text = one + " + " + two;
    }

    private void GenerateAnswer()
    {
        var answers = new int[stagesetting[(stage-10) / 10]];
        answers[0] = answer;
        if (r.Next(2) == 1) //dudz
        {
            if (answer < 12)
            {
                answers[1] = r.Next(1, 13);
            }
            else
            {
                answers[1] = Math.Max(1, answer - r.Next(1, 13));
            }
        } else
        {
            if (answer == upperbound - 1)
            {
                answers[1] = answer - 10;
            }
            else
            {
                answers[1] = Math.Min(upperbound - 1, answer + r.Next(1, 13));
            }
        }
        for (int i = 2; i < answers.Length;)
        {
            answers[i] = r.Next(1, upperbound);
            //make extra sure that there's no duplicate
            for (int j = 0; j < i; j++)
            {
                if (answers[j] == answers[i])
                {
                    i--;
                    j = i;
                }
            }
            i++;
        }
        Shuffle(answers);
        switch (answers.Length)
        {
            case 2:
                active = a2;
                break;
            case 4:
                active = a4;
                break;
            case 6:
                active = a6;
                break;
            case 9:
                active = a9;
                break;
            default:
                break;
        }
        active.GetComponentsInChildren<Text>()[0].text = (stage/10)+"-"+((stage%10)+1);
        for (int i = 0; i < answers.Length; i++)
        {
            var colors = active.GetComponentsInChildren<Button>()[i].GetComponentsInChildren<Image>()[0];
            colors.color = Color.white;
            active.GetComponentsInChildren<Button>()[i].onClick.RemoveAllListeners();
            var ani = answers[i];
            active.GetComponentsInChildren<Button>()[i].onClick.AddListener(() => { CheckAnswer(ani); });
            active.GetComponentsInChildren<Button>()[i].GetComponentsInChildren<Text>()[0].text = ani.ToString();
        }
        time = 180 + 120 * ((stage/10)%2) + 60;
        question.GetComponent<Animation>().Play("SlideUp");
        active.GetComponent<Animation>().Play("SlideIn");
    }
    
    private void Shuffle<T>(T[] array)
    {
        int n = array.Length;
        while (n > 1)
        {
            int k = r.Next(n--);
            T temp = array[n];
            array[n] = array[k];
            array[k] = temp;
        }
    }
}
